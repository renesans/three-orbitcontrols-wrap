# three-orbitcontrols-wrap

THREE.js OrbitControls wrapper for npms module

With yarn
```bash
yarn add three-orbitcontrols-wrap
```

With npm
```bash
npm i three-orbitcontrols-wrap
```

Usage

```es6
    import * as THREE from 'three';
    import {OrbitControlsWrap} from "three-orbitcontrols-wrap";

    OrbitControlsWrap(THREE);

    // now use THREE.OrbitControls
```